import { Container, Grid } from "@mantine/core";
import { useInterval } from "@mantine/hooks";
import { useEffect, useState } from "react";

const Logs = () => {
  const [logs, setLogs] = useState([]);
  const handleGetData = async () => {
    const res = await fetch("/api/hello");
    const data = await res.json();
    setLogs(data.logs);
  };
  useEffect(() => {
    const id = setInterval(() => {
      handleGetData();
    }, 1000);
    return () => clearInterval(id);
  }, []);
  return (
    <Grid>
      <Grid.Col>Logs: </Grid.Col>
      {logs.map((val, key) => {
        return <Grid.Col key={key}>{val}</Grid.Col>;
      })}
    </Grid>
  );
};

export default Logs;
