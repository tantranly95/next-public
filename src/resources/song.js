import {
  Edit,
  SimpleForm,
  TextInput,
  ImageInput,
  ImageField,
  FileInput,
  FileField,
  Create,
  List,
  Datagrid,
  TextField,
  DeleteButton,
  EditButton,
  DateField,
  BulkUpdateButton,
  BulkDeleteButton,
  useListContext,
  useCreate,
  useRefresh,
  useNotify,
  useUnselectAll,
  Button,
} from "react-admin";
import { useState } from "react";
import { PlusOne, AddCircleOutline } from "@mui/icons-material";
const CustomImageField = (props) => {
  console.log(props);
  const { source, ...rest } = props;
  return (
    <ImageField source={source} src={(record) => record[source]} {...rest} />
  );
};

const SongBulkActionButtons = () => {
  const { selectedIds, resource } = useListContext();
  const [playlistName, setPlaylistName] = useState("New playlist");
  const refresh = useRefresh();
  const notify = useNotify();
  const unselectAll = useUnselectAll(resource);
  const [create, {}] = useCreate();
  const handleCreate = async () => {
    create(
      "playlists",
      {
        data: {
          songs: selectedIds,
          title: playlistName,
        },
      },
      {
        onSuccess: () => {
          refresh();
          notify("Posts updated");
          unselectAll();
        },
        onError: (error) =>
          notify("Error: posts not updated", { type: "error" }),
      }
    );
  };
  console.log(selectedIds, resource, playlistName);
  return (
    <>
      <span>
        <input
          placeholder="Enter new playlist name"
          onChange={(e) => setPlaylistName(e.target.value)}
        ></input>
      </span>
      <Button onClick={handleCreate}>Create</Button>
      {/* default bulk delete action */}
    </>
  );
};

export const SongsList = (props) => (
  <List {...props}>
    <Datagrid bulkActionButtons={<SongBulkActionButtons />}>
      <TextField source="title" />
      <DateField source="createdate" />
      <ImageField source="imageURL" title="imageURL" />
      <EditButton />
      <DeleteButton />
    </Datagrid>
  </List>
);
export const SongsEdit = (props) => {
  console.log(props);
  return (
    <Edit {...props}>
      <SimpleForm>
        <TextInput source="title" />
        <TextInput source="author" />
        {/* <ReferenceInput label="Comment" source="title" reference="comments">
        <SelectInput optionText="title" />
      </ReferenceInput> */}
        <FileInput source="audioURL" label="File" accept="audio/*">
          <FileField source="src" title="title" />
        </FileInput>
        <ImageField source="imageURL" title="Current image" />
        <ImageInput source="imageURL" label="Image" accept="image/*">
          <ImageField source="src" title="Current image" />
        </ImageInput>
      </SimpleForm>
    </Edit>
  );
};

export const SongsCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="title" />
      <TextInput source="author" />
      {/* <ReferenceInput label="Comment" source="title" reference="comments">
        <SelectInput optionText="title" />
      </ReferenceInput> */}
      <FileInput source="audioURL" label="File" accept="audio/*">
        <FileField source="src" title="title" />
      </FileInput>
      <ImageInput source="imageURL" label="Image" accept="image/*">
        <ImageField source="src" title="title" />
      </ImageInput>
    </SimpleForm>
  </Create>
);
