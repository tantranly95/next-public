import Logs from "@/components/Logs";
import YoutubeDownload from "@/components/YoutubeDownload";
import { Container } from "@mantine/core";

const Youtube = () => {
  return (
    <Container size="xs" p="xl">
      <YoutubeDownload />
      <Logs />
    </Container>
  );
};

export default Youtube;
