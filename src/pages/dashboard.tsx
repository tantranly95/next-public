import dynamic from "next/dynamic";
const App = dynamic(() => import("../appwrite/dashboard/appwrite"), {
  ssr: false,
});
const Dashboard = () => {
  return <App />;
};

export default Dashboard;
