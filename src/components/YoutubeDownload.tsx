import {
  Button,
  Container,
  Grid,
  Select,
  Modal,
  JsonInput,
  Space,
  Textarea,
} from "@mantine/core";
import { useEffect, useState } from "react";
import { notifications } from "@mantine/notifications";
import { useRouter } from "next/router";

const toastError = (msg: string) => {
  notifications.show({
    title: "Lỗi",
    message: msg,
    color: "red",
  });
};
const toastSuccess = (msg: string) => {
  notifications.show({
    title: "Hoàn thành",
    message: msg,
    color: "green",
  });
};

const YoutubeDownload = () => {
  const [config, setConfig] = useState<any>(null);
  const [youtubeUrl, setYoutubeUrl] = useState("");
  const router = useRouter();
  const handleGet = () => {
    let localConfig: any;
    try {
      localConfig = JSON.parse(localStorage.getItem("projectSelected") || "{}");
      if (!localConfig.projectId) {
        toastError("Chưa chọn project");
        router.push("/");
      }
      return localConfig;
    } catch (err) {
      toastError("Chưa chọn project");
      router.push("/");
    }
  };
  const handleSubmitUrl = async () => {
    try {
      if (!config) {
        return toastError("Vui lòng chọn project config");
      }
      if (!youtubeUrl) {
        return toastError("Vui lòng điền link youtube");
      }
      await fetch("/api/hello/", {
        method: "POST",
        body: JSON.stringify({ ...config, youtubeUrl }),
      });
      toastSuccess("Server đang tiến hành convert và upload lên appwrite");
      setYoutubeUrl("");
    } catch (err) {
      console.log("err");
      console.log(err);
    }
  };
  useEffect(() => {
    const localConfig = handleGet();
    console.log("local config", localConfig);
    setConfig(localConfig);
  }, []);
  return (
    <>
      <Grid align="flex-end" justify="space-between">
        <Grid.Col span={8}>
          <Select
            label="Project: "
            placeholder={config?.projectName || "Chưa chọn project"}
            readOnly
            data={[]}
            value={config?.projectName || "Chưa chọn project"}
          />
        </Grid.Col>
        <Grid.Col span={4}>
          <Button onClick={() => router.push("/")}>Trở về trang chủ</Button>
        </Grid.Col>
        <Grid.Col span={12}>
          <Textarea
            placeholder="Nhập link youtube"
            value={youtubeUrl}
            onChange={(event) => setYoutubeUrl(event.currentTarget.value)}
          />
        </Grid.Col>
        <Grid.Col>
          <Button onClick={handleSubmitUrl}>Bắt đầu download</Button>
        </Grid.Col>
      </Grid>
    </>
  );
};

export default YoutubeDownload;
