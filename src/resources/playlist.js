import {
  Edit,
  SimpleForm,
  TextInput,
  ImageInput,
  ImageField,
  FileInput,
  FileField,
  Create,
  ReferenceArrayInput,
  SelectArrayInput,
  List,
  Datagrid,
  TextField,
  EditButton,
  DeleteButton,
  Show,
  SimpleShowLayout,
  ReferenceArrayField,
  ReferenceField,
  ShowButton,
  SelectInput,
  CheckboxGroupInput,
  SingleFieldList,
  ChipField,
} from "react-admin";
export const PlaylistsShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="title" />
      <ImageField source="imageURL" title="imageURL" />
      <ReferenceArrayField
        label="Songs"
        reference="songs"
        source="songs"
        perPage={100}
      >
        <SingleFieldList>
          <ChipField source="title" />
        </SingleFieldList>
      </ReferenceArrayField>
    </SimpleShowLayout>
  </Show>
);
export const PlaylistsCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="title" />
      <ReferenceArrayInput
        perPage={100}
        source="songs"
        reference="songs"
        allowEmpty
      >
        <CheckboxGroupInput optionText="title"></CheckboxGroupInput>
      </ReferenceArrayInput>
      <ImageInput source="imageURL" label="Image" accept="image/*">
        <ImageField source="src" title="title" />
      </ImageInput>
    </SimpleForm>
  </Create>
);

export const PlaylistsEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="title" />
      <ReferenceArrayInput
        perPage={100}
        source="songs"
        reference="songs"
        allowEmpty
      >
        <CheckboxGroupInput optionText="title"></CheckboxGroupInput>
      </ReferenceArrayInput>
      <ImageField source="imageURL" />
      <ImageInput source="imageURL" label="Image" accept="image/*">
        <ImageField source="src" title="title" />
      </ImageInput>
    </SimpleForm>
  </Edit>
);
export const PlaylistsList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="title" />
      <ShowButton />
      <EditButton />
      <DeleteButton />
    </Datagrid>
  </List>
);
