import {
  CreateParams,
  CreateResult,
  DeleteResult,
  GetListParams,
  GetListResult,
  GetManyReferenceResult,
  GetManyResult,
  GetOneResult,
  UpdateParams,
  UpdateResult,
  ValidUntil,
  DataProvider,
  GetOneParams,
  GetManyParams,
  GetManyReferenceParams,
  UpdateManyParams,
  DeleteParams,
  DeleteManyParams,
  Identifier,
  RaRecord,
} from "ra-core";
import Resizer from "react-image-file-resizer";

import { Client, Databases, Query, ID, Storage } from "appwrite";
import configs from "../configs";

const resizeFile = (file: any) =>
  new Promise((resolve) => {
    Resizer.imageFileResizer(
      file,
      configs.maxWidth,
      configs.maxHeight,
      "JPEG",
      configs.quality,
      0,
      (uri) => {
        resolve(uri);
      },
      "file"
    );
  });

export class AppwriteDataProvider implements DataProvider {
  databases: Databases;
  resources: { [resource: string]: string };
  storage: Storage;
  bucketId: string;
  databaseId: string;

  constructor(
    client: Client,
    resources: { [resource: string]: string },
    databaseId: string,
    bucketId: string
  ) {
    this.databases = new Databases(client);
    this.resources = resources;
    this.storage = new Storage(client);
    this.bucketId = bucketId;
    this.databaseId = databaseId;
  }
  async uploadFile(file: File) {
    //start upload
    console.log("start upload: ", file);
    const fileResponse = await this.storage.createFile(
      this.bucketId,
      ID.unique(),
      file
    );
    const fileURL = await this.storage.getFileView(
      this.bucketId,
      fileResponse.$id
    );
    console.log("success upload: ", fileURL);
    return fileURL;
  }
  async convertFileFields(data: any) {
    const newData = { ...data };
    console.log("convert file field");
    const promises = Object.keys(newData).map(async (key) => {
      const value = newData[key];
      if (value?.rawFile instanceof File) {
        if (key.indexOf("image") != -1 && configs.isResize) {
          const newFile = await resizeFile(value.rawFile);
          newData[key] = await this.uploadFile(newFile as File);
        } else {
          newData[key] = await this.uploadFile(value.rawFile);
        }
      }
    });

    await Promise.all(promises);

    return newData;
  }

  async getList<RecordType extends RaRecord = any>(
    resource: string,
    params: GetListParams
  ): Promise<GetListResult<RecordType>> {
    const { page, perPage } = params.pagination;
    const { field, order } = params.sort;
    const filter = params.filter;
    let queries: string[] = [];
    for (let key in filter) {
      queries = [...queries, Query.equal(key, filter[key])];
    }
    console.log("pag: ", page, perPage);
    const documentsList = await this.databases.listDocuments(
      this.databaseId,
      this.resources[resource],
      [
        ...queries,
        Query.limit(perPage),
        Query.offset((page - 1) * perPage),
        // order == "ASC" ? Query.orderAsc(field) : Query.orderDesc(field),
      ]
    );

    const records = documentsList.documents.map((d) => {
      const { $id } = d;
      return {
        ...d,
        id: $id,
      };
    });
    console.log(documentsList);

    return {
      data: records as any[],
      total: documentsList.total,
    };
  }

  async getOne<RecordType extends RaRecord = any>(
    resource: string,
    params: GetOneParams
  ): Promise<GetOneResult<RecordType>> {
    const { id } = params;

    const document = await this.databases.getDocument(
      this.databaseId,
      this.resources[resource],
      `${id}`
    );

    return {
      data: { id: document.$id, ...document } as any,
    };
  }

  async getMany<RecordType extends RaRecord = any>(
    resource: string,
    params: GetManyParams
  ): Promise<GetManyResult<RecordType>> {
    const promises = params.ids.map((id) => {
      return this.databases.getDocument(
        this.databaseId,
        this.resources[resource],
        `${id}`
      );
    });

    const documents = await Promise.all(promises);

    const records = documents.map((d) => {
      const { $id } = d;
      return {
        id: $id,
        ...d,
      };
    });

    return {
      data: records as any[],
    };
  }

  async getManyReference<RecordType extends RaRecord = any>(
    resource: string,
    params: GetManyReferenceParams
  ): Promise<GetManyReferenceResult<RecordType>> {
    const { target, id, pagination, sort, filter } = params;

    const filters = [Query.equal(target, id)];
    Object.keys(filter).forEach((key) => {
      filters.push(Query.equal(key, filter[key]));
    });

    const documentsList = await this.databases.listDocuments(
      this.databaseId,
      this.resources[resource],
      [
        ...filters,
        Query.limit(pagination.perPage),
        Query.offset((pagination.page - 1) * pagination.perPage),
        // sort.order === "ASC"
        //   ? Query.orderAsc(sort.field)
        //   : Query.orderDesc(sort.field),
      ]
    );

    const records = documentsList.documents.map((d) => {
      const { $id } = d;
      return {
        id: $id,
        ...d,
      };
    });

    return {
      data: records as any[],
      total: documentsList.total,
    };
  }

  async update<RecordType extends RaRecord = any>(
    resource: string,
    params: UpdateParams
  ): Promise<UpdateResult<RecordType>> {
    const {
      $id,
      $collectionId,
      $createdAt,
      $updatedAt,
      $databaseId,
      $permissions,
      id,
      ...data
    } = params.data;

    let newData;
    for (let k in data) {
      if (data[k]?.rawFile) {
        newData = await this.convertFileFields(data);
        break;
      }
    }

    const document = await this.databases.updateDocument(
      this.databaseId,
      this.resources[resource],
      $id,
      newData || data
    );

    return {
      data: { id: $id, ...document } as any,
    };
  }

  async updateMany(
    resource: string,
    params: UpdateManyParams
  ): Promise<{
    data?: string[];
    validUntil?: ValidUntil;
  }> {
    const { ids, data } = params;
    const {
      $id,
      $collectionId,
      $permissions,
      $createdAt,
      $updatedAt,
      $databaseId,
      ...rest
    } = data;

    const promises = ids.map((id) => {
      return this.databases.updateDocument(
        this.databaseId,
        this.resources[resource],
        `${id}`,
        rest
      );
    });

    const documents = await Promise.all(promises);

    return {
      data: documents.map((d) => d.$id),
    };
  }

  async create<RecordType extends RaRecord = any>(
    resource: string,
    params: CreateParams
  ): Promise<CreateResult<RecordType>> {
    const { $id, $collectionId, $permissions, ...data } = params.data;
    console.log("parmas create: ", params.data);
    let newData;
    for (let k in data) {
      if (data[k]?.rawFile) {
        newData = await this.convertFileFields(data);
        break;
      }
    }

    const document = await this.databases.createDocument(
      this.databaseId,
      this.resources[resource],
      ID.unique(),
      newData || data
    );
    return {
      data: {
        id: document.$id,
        ...document,
      } as any,
    };
  }

  async delete<RecordType extends RaRecord = any>(
    resource: string,
    params: DeleteParams
  ): Promise<DeleteResult<RecordType>> {
    await this.databases.deleteDocument(
      this.databaseId,
      this.resources[resource],
      `${params.id}`
    );

    return {
      data: params.previousData as any,
    };
  }

  async deleteMany(
    resource: string,
    params: DeleteManyParams
  ): Promise<{ data?: Identifier[] }> {
    const promises = params.ids.map((id) => {
      return this.databases.deleteDocument(
        this.databaseId,
        this.resources[resource],
        `${id}`
      );
    });

    await Promise.all(promises);

    return {
      data: params.ids,
    };
  }
}
