export default {
  projectId: process.env.REACT_APP_APPWRITE_PROJECT_ID || "",
  endpoint: process.env.REACT_APP_APPWRITE_API_ENDPOINT || "",
  databaseId: process.env.REACT_APP_APPWRITE_DATABASE_ID || "",
  songs: process.env.REACT_APP_APPWRITE_SONGS_ID || "",
  playlists: process.env.REACT_APP_APPWRITE_PLAYLISTS_ID || "",
  bucketId: process.env.REACT_APP_APPWRITE_BUCKET_ID || "",
  isResize: true,
  maxWidth: 1000,
  maxHeight: 1000,
  quality: 80,
};
