import {
  Admin,
  EditGuesser,
  ListGuesser,
  Resource,
  ShowGuesser,
} from "react-admin";
import { AppwriteDataProvider } from "../providers/appwrite";
import { AppwriteAuthProvider } from "../auth/index";
import client from "../clients/appwrite";
import { SongsCreate, SongsEdit, SongsList } from "../../resources/song";
import {
  PlaylistsCreate,
  PlaylistsEdit,
  PlaylistsList,
  PlaylistsShow,
} from "../../resources/playlist";
import configs from "../configs";
import { Router } from "next/router";
const projectData = localStorage.getItem("projectSelected") || "";
const project = JSON.parse(projectData);
const RESOURCES = {
  songs: project.songCollectionId,
  playlists: project.playlistCollectionId,
};
client.setEndpoint(project.endpoint).setProject(project.projectId);
const dataProvider = new AppwriteDataProvider(
  client,
  RESOURCES,
  project.databaseId,
  project.bucketId
);
const authProvider = new AppwriteAuthProvider(client);
const App = (): JSX.Element => (
  <Admin
    authProvider={authProvider}
    dataProvider={dataProvider}
    loginPage={false}
  >
    <Resource
      name="songs" // Matches resources key
      list={SongsList}
      edit={SongsEdit}
      create={SongsCreate}
    />
    <Resource
      name="playlists"
      list={PlaylistsList}
      create={PlaylistsCreate}
      show={PlaylistsShow}
      edit={PlaylistsEdit}
    />
  </Admin>
);
export default App;
