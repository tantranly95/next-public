import {
  Button,
  Container,
  Grid,
  Select,
  Modal,
  JsonInput,
  Space,
  Textarea,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { useEffect, useState } from "react";
import { notifications } from "@mantine/notifications";
import client from "../appwrite/clients/appwrite";
import { Account } from "appwrite";
import { useRouter } from "next/router";

const toastError = (msg: string) => {
  notifications.show({
    title: "Lỗi",
    message: msg,
    color: "red",
  });
};
const toastSuccess = (msg: string) => {
  notifications.show({
    title: "Hoàn thành",
    message: msg,
    color: "green",
  });
};

const SelectConfig = () => {
  const router = useRouter();
  const [opened, { open, close }] = useDisclosure(false);
  const [index, setIndex] = useState(null);
  const [json, setJson] = useState("");
  const [configs, setConfigs] = useState<any>([]);
  const handleJson = () => {
    let data: any;
    try {
      data = JSON.parse(json);
    } catch (err) {}
    if (!data) {
      return toastError("Không đúng định dạng JSON");
    }
    if (!data.web) {
      return toastError("Dùng cli version mới nhất để tạo config");
    }
    if (!data.projectId) {
      return toastError("Thiếu projectId");
    }
    if (!data.endpoint) {
      return toastError("Thiếu endpoint");
    }
    if (!data.databaseId) {
      return toastError("Thiếu databaseId");
    }
    if (!data.songCollectionId) {
      return toastError("Thiếu songCollectionId");
    }
    if (!data.bucketId) {
      return toastError("Thiếu bucketId");
    }
    if (!data.secret) {
      return toastError("Thiếu secret");
    }
    const newConfigs = [...configs, data];
    setConfigs(newConfigs);
    handleSave(newConfigs);
    close();
  };
  const handleSave = (newConfigs: any) => {
    localStorage.setItem("configs", JSON.stringify(newConfigs));
  };
  const handleGet = () => {};
  const handleSubmitUrl = async () => {
    try {
      const config = configs[index == null ? 999 : index];
      if (!config) {
        return toastError("Vui lòng chọn project");
      }
      localStorage.setItem("projectSelected", JSON.stringify(config));
      router.push("/youtube");
    } catch (err) {
      console.log("err");
      console.log(err);
    }
  };
  const handleSelectChange = (value: any) => {
    console.log(value);
    setIndex(value);
  };
  useEffect(() => {
    let localConfig: any = [];
    try {
      const configInLocalStroage = JSON.parse(
        localStorage.getItem("configs") || "[]"
      );
      for (let config of configInLocalStroage) {
        if (config.web) {
          localConfig = [...localConfig, config];
        }
      }
      localStorage.setItem("configs", JSON.stringify(localConfig));
    } catch (err) {}
    setConfigs(localConfig);
  }, []);
  const handleAdminManager = async () => {
    try {
      const config = configs[index == null ? 999 : index];
      if (!config) {
        return toastError("Vui lòng chọn project");
      }
      client.setEndpoint(config.endpoint).setProject(config.projectId);
      const account = new Account(client);
      await account.createEmailSession(config.userEmail, config.userPassword);
      toastSuccess("Đăng nhập thành công");
      localStorage.setItem("projectSelected", JSON.stringify(config));
      router.push("/dashboard");
    } catch (err) {
      console.log(err);
      toastError("Lỗi");
    }
  };
  const options = configs.map((val: any, index: any) => ({
    value: index,
    label: val.projectName,
  }));
  const handleReset = () => {
    localStorage.removeItem("configs");
    setConfigs([]);
  };
  return (
    <>
      <Modal opened={opened} onClose={close} title="Add config">
        {/* Modal content */}
        <JsonInput
          label="Config dưới dạng JSON"
          placeholder="{'key':'value'}"
          validationError="Không đúng định dạng"
          formatOnBlur
          autosize
          minRows={4}
          value={json}
          onChange={(value) => {
            setJson(value);
          }}
        />
        <Space h="md" />
        <Button onClick={handleJson}>Add</Button>
      </Modal>
      <Grid align="flex-end" justify="space-between">
        <Grid.Col span={6}>
          <Select
            label="Chọn project"
            placeholder="Chọn project"
            data={options}
            onChange={handleSelectChange}
          />
        </Grid.Col>
        <Grid.Col span={3}>
          <Button onClick={open}>Thêm project</Button>
        </Grid.Col>
        <Grid.Col span={3}>
          <Button color="red" onClick={handleReset}>
            Xóa tất cả
          </Button>
        </Grid.Col>

        <Grid.Col>
          <Button onClick={handleAdminManager}>Đi đến admin manager</Button>
        </Grid.Col>
        <Grid.Col>
          <Button onClick={handleSubmitUrl}>Đi đến Youtube Downloader</Button>
        </Grid.Col>
      </Grid>
    </>
  );
};

export default SelectConfig;
