// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import ffmpegPath from "ffmpeg-static";
import ffmpeg from "fluent-ffmpeg";
import ytdl from "@distube/ytdl-core";
import fs from "fs";
import { Client, InputFile, Storage, Databases } from "node-appwrite";
const outputFile = `output.mp3`;
const outputAudioFile = `audio.mp3`;
ffmpeg.setFfmpegPath(ffmpegPath as string);
let logs: string[] = [];
const log = (msg: any) => {
  if (logs.length > 1000) {
    logs.pop();
  }
  logs.unshift(msg);
};

type Data = {
  endpoint: string;
  projectId: string;
  databaseId: string;
  songCollectionId: string;
  bucketId: string;
  secret: string;
  youtubeUrl: string;
};
let itemQueue: {
  queues: Data[];
  isWorking: boolean;
  addItem: (item: Data) => void;
  worker: Function;
  downloader: Function;
} = {
  queues: [],
  isWorking: false,
  addItem(item: Data) {
    this.queues.push(item);
    if (this.isWorking) return;
    this.isWorking = true;
    this.worker();
  },
  async worker() {
    if (this.queues.length == 0) {
      this.isWorking = false;
      return;
    }
    await this.downloader();
    return this.worker();
  },
  async downloader() {
    const data = this.queues.shift();
    try {
      log(`Bắt đầu download video: ${data?.youtubeUrl}`);
      await new Promise((rel, rej) => {
        ytdl(data?.youtubeUrl || "", {
          filter: "audioonly",
          quality: "highestaudio",
        })
          .on("progress", (chunk, downloaded, total) => {
            console.log("Đang tải về: ", downloaded / total, "%");
            log(`Đang tải về: ${downloaded / total} %`);
          })
          .on("end", () => {
            rel("done");
          })
          .on("error", (err) => {
            rej(err);
          })
          .pipe(fs.createWriteStream(outputFile));
      });
      log(`Bắt đầu chuyển đổi sang mp3`);
      await new Promise((resolve, reject) => {
        ffmpeg(outputFile)
          //.audioCodec("libmp3lame")
          .audioBitrate("256k")
          .format("mp3")
          .on("end", () => {
            console.log("Finished converting audio to MP3");
            resolve("done");
          })
          .on("error", (err) => {
            console.error("Error converting audio to MP3:", err);
            reject(err);
          })
          .on("progress", (chunk, current, total) => {
            log(`Đang chuyển đổi sang mp3: ${chunk.targetSize} kbps`);
            console.log("Đang chuyển đổi: ", chunk, current, total);
          })
          .save(outputAudioFile);
      });
      log(`Convert video thành audio thành công`);
      fs.unlinkSync(outputFile);
      const client = new Client();
      client
        .setEndpoint(data?.endpoint || "")
        .setProject(data?.projectId || "")
        .setKey(data?.secret || "");
      const storage = new Storage(client);
      log(`Upload audio lên server`);
      let response: any = await storage.createFile(
        data?.bucketId || "",
        "unique()",
        InputFile.fromPath(outputAudioFile, "output.mp3")
      );
      const url = `${data?.endpoint}/storage/buckets/${data?.bucketId}/files/${response.$id}/view?project=${data?.projectId}`;
      const metadata = await ytdl.getBasicInfo(data?.youtubeUrl || "");
      const databases = new Databases(client);
      log(`Tạo song document: ${metadata.videoDetails.title}`);
      response = await databases.createDocument(
        data?.databaseId || "",
        data?.songCollectionId || "",
        "unique()",
        {
          audioURL: url,
          title: metadata.videoDetails.title,
          author: "default",
        }
      );
      log(`Hoàn thành: ${data?.youtubeUrl}`);
      fs.unlinkSync(outputAudioFile);
    } catch (err) {
      console.log(err);
      log(`Lỗi: ${data?.youtubeUrl} - Vui lòng thử lại`);
      try {
        fs.unlinkSync(outputFile);
      } catch (error) {}
      try {
        fs.unlinkSync(outputAudioFile);
      } catch (error) {}
    }
  },
};
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  // Convert the video to an MP3
  try {
    const requestMethod = req.method;
    switch (requestMethod) {
      case "POST":
        const body = JSON.parse(req.body);
        itemQueue.addItem(body as Data);
        res.status(200).json({ message: `Success` });
        break;

      // handle other HTTP methods
      default:
        return res.status(200).json({ logs: logs });
    }
  } catch (err) {
    console.log(err);
    res.status(200).json({ name: "error" });
  }
}
